#!/usr/bin/python

# This software is distributed under the MIT License.
# The MIT License (MIT)

# Copyright (c) 2014 Nils Deppe

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# This code reads an index from WordNet
# and extracts all of the "words" to create
# a word database.


# Import system functions
import sys;
# re is used for regexp
import re;
# Warn about overwriting existing database.
warningDecision = raw_input("WARNING: Make sure you have a copy of the current noun.db file as this will erase it! To abort type EXIT.\n");
if re.match("EXIT", warningDecision):
    sys.exit();

# Get input name of database.
inputName = raw_input("Enter the name of the database to parse: ");

# Open files
nounDbInput = open(inputName, 'r'); #Open input file
nounDbOutput = open('noun.db', 'w'); #Open output file
inputNouns = nounDbInput.readlines(); #Read all the lines in
# Now we parse the lines
for line in inputNouns:
    words = line.split(" ");
    if words[0] : # check to make sure we are not an empty string.
        nounDbOutput.write(words[0].replace("_", " "));
        nounDbOutput.write("\n");

nounDbInput.close();
nounDbOutput.close();



