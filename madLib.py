#!/usr/bin/python

# This software is distributed under the MIT License.
# The MIT License (MIT)

# Copyright (c) 2014 Nils Deppe

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# re is used for regexp
import re;
# Used for randomly accessing the array of nouns.
import random;

# This returns the number of lines in a file.
def fileLength(fName):
    with open(fName) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

# This returns all of the lines in the database file.
# It removes the \n and \r characters from the end.
def loadDb(fName):
    with open(fName) as f:
        return [line.rstrip() for line in f]

# This searches for possible input parameters.
def inputSearch(inputText):
    help = re.search('-h', inputText);
    if help:
        print ("\nYou can enter a sentence where you replace "
               "nouns by one of the following:");
        print "   [noun]   = a random noun takes this place.";
        print ("   [nouns]  = a random noun followed by an s, e.g. "
               "goats takes this place.");
        print ("   [noun's] = similar to above, except now it is "
               "'s. So goat's takes this place.\n");
        print ("   [noun-'] = similar to above, except now it is "
               "plural possesive. The goats' food.\n");
        return raw_input("Enter the sentence you want to parse: ");

# Fix up first word, if it is not capitalized.
# Also, match period, one or mare spaces, small letter.
def capitalizeWords(sentence):
    sentence = sentence[0].upper() + sentence[1:];
    # Fix up any words that start sentences that aren't capitalized.
    correctCapitalization = re.search('\.\s*[a-z]', sentence);
    while correctCapitalization:
        sentence = re.sub('\.\s*([a-z])', \
                              correctCapitalization.group(0).upper(), \
                              sentence, 1);
        correctCapitalization = re.search('\.\s*[a-z]', sentence);

    return sentence;

# Fix up any issues with "a apple"
def correctAnIssues(sentence):
    anCorrection = re.search('\A[a]\s[AaEeIiOoUu]', sentence);
    if anCorrection:
        anCorrection = re.sub('\A[a]\s', 'an ', anCorrection.group(0), 1);
        sentence = re.sub('\A([a])\s[AaEeIiOoUu]', anCorrection, \
                              sentence, 1);
    
    anCorrection = re.search('\s[Aa]\s[AaEeIiOoUu]', sentence);
    while anCorrection:
        anCorrection = re.sub('\s[a]\s', ' an ', anCorrection.group(0), 1);
        sentence = re.sub('\s([a])\s[AaEeIiOoUu]', anCorrection, \
                              sentence, 1);
        anCorrection = re.search('\s[a]\s[AaEeIiOoUu]', sentence);

    return sentence;

# Correct any words that end in ies that shouldn't. All countries,
# and some
def correctWrongEndings(sentence):
    # Now fix up the pluralizations of inherently singular words
    plurDb = loadDb('replacements.db');
    for lines in plurDb:
        words = lines.split(" ");
        # Ignore case
        line = re.compile(words[0], re.IGNORECASE);
        wrongPlur = re.search(line, sentence);
        while wrongPlur:
            if re.match('RANDOM', words[1]):
                sentence = re.sub(line, nouns[random.randint(0,nounDbLength-1)]+'s',\
                                      sentence, 1);
            else:
                sentence = re.sub(line, words[1],\
                                      sentence, 1);
            wrongPlur = re.search(line, sentence);
    # Now we fix up "sss" endings
    wrongSss = re.search("sss", sentence);
    while wrongSss:
        sentence = re.sub("sss", "sses", sentence);
        wrongSss = re.search("sss", sentence);
    # Now fix up "ss" endings
    wrongSs = re.search("([bcdfghjklmnpqrstvwxz])ss", sentence);
    while wrongSs:
        sentence = re.sub("([bcdfghjklmnpqrstvwxz])ss", wrongSs.group(1)+'s', \
                              sentence, 1);
        wrongSs = re.search("([bcdfghjklmnpqrstvwxz])ss", sentence);
    # Fix hs ending
    wrongHs = re.search("hs", sentence);
    while wrongHs:
        sentence = re.sub("hs", "hes", sentence, 1);
        wrongHs = re.search("hs", sentence);
    # Fix xs ending
    wrongXs = re.search("xs", sentence);
    while wrongXs:
        sentence = re.sub("xs", "xes", sentence, 1);
        wrongXs = re.search("xs", sentence);
    return sentence;


# Now fix up any ys
def correctIes(sentence):
    sentence = re.sub("ys", "ies", sentence);
    # Fix up any issues with [vowel]ies to ys
    wrongIes = re.search("([AaEeIiOoUu])ies", sentence);
    while wrongIes:
        sentence = re.sub("[AaEeIiOoUu]ies", \
                              wrongIes.group(1)+'ys', \
                              sentence, 1);
        wrongIes = re.search("([AaEeIiOoUu])ies", sentence);
    return sentence;

def replaceNouns(sentence, nouns, nounDbLength):
    # Now for replace each [noun].
    # The loop goes until [noun] is no longer in the "sentence"
    nounReplace = re.search("\[noun\]", sentence);
    while nounReplace:
        # Do random number generation.
        random.seed();
        replacementNoun = nouns[random.randint(0,nounDbLength-1)];
        replacementNoun = correctWrongEndings(replacementNoun);
        sentence = re.sub("\[noun\]",replacementNoun, sentence, 1);
        nounReplace = re.search("\[noun\]", sentence);

    # Now for replace each [nouns].
    nounReplace = re.search("\[nouns\]", sentence);
    while nounReplace:
        random.seed();
        replacementNoun = nouns[random.randint(0,nounDbLength-1)]+'s';
        replacementNoun = correctWrongEndings(replacementNoun);
        sentence = re.sub("\[nouns\]", replacementNoun, sentence, 1);
        nounReplace = re.search("\[nouns\]", sentence);

    # Now for replace each [noun's].
    nounReplace = re.search("\[noun's\]", sentence);
    while nounReplace:
        random.seed();
        replacementNoun = nouns[random.randint(0,nounDbLength-1)]+'\'s';
        replacementNoun = correctWrongEndings(replacementNoun);
        sentence = re.sub("\[noun's\]", replacementNoun, sentence, 1);
        nounReplace = re.search("\[noun's\]", sentence);

    # Now for replace each [nouns'].
    nounReplace = re.search("\[nouns'\]", sentence);
    while nounReplace:
        random.seed();
        replacementNoun = nouns[random.randint(0,nounDbLength-1)]+'s\'';
        replacementNoun = correctWrongEndings(replacementNoun);
        sentence = re.sub("\[nouns'\]", replacementNoun, sentence, 1);
        nounReplace = re.search("\[nouns'\]", sentence);
    return sentence;

#############################################################
# Main program starts
print "Type -h as the sentence to see help."
sentence = raw_input("Enter the sentence you want to parse: ");
# Search for input parameters.
sentenceFromHelp = inputSearch(sentence);
if sentenceFromHelp:
    sentence = sentenceFromHelp;
# Get the length of the noun database.
nounDbLength = fileLength("noun.db");
# Load the noun data base.
nouns = loadDb("noun.db");

# Replace nouns
sentence = replaceNouns(sentence, nouns, nounDbLength);

# Fix grammatical issues
sentence = correctIes(sentence);
sentence = correctAnIssues(sentence);
sentence = capitalizeWords(sentence);

# Print out the result.
print "\n";
print sentence;
print "\n";
