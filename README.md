PyMadLib
========

Requirements
------------

1. Python

    Mac OS 10.2 and newer:
    Nothing to install. Should run just fine. If you have trouble,
    see [here](https://wiki.python.org/moin/BeginnersGuide/Download).
    You may also use Homebrew to install python.

    Ubuntu:
    Install the python and python-tk packages using
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install python python-tk
    See [here](https://wiki.python.org/moin/BeginnersGuide/Download)
    for details.

    Windows:
    For Windows you can find a suitable download
    [here](https://www.python.org/downloads/windows/)
    Either version 2.x or 3.x should work. The code
    does not use any fancy features. I recommend version 2.

    Downloading and Installing
    1. [Python](https://www.python.org/download)
    2. Choose either the 2.7.x python installer and download it
    3. Locate it in your downloads folder and double click. Select "Run" and
    follow the setup instructions

2. Noun database

    Included is a database of "common" nouns, which
    contains ~2328 nouns. This should be sufficient
    to get you laughing (or confused why anyone thinks this is funny).
    However, if you are so inclined to, you may install a "more complete"
    database from WordNet. This is ~117800 nouns. This is not included due
    to licensing and due to the fact that there are many uncommon nouns,
    which leads to confusion instead of laughter.

    To install the WordNet database, visit
    [this site](http://wordnet.princeton.edu/wordnet/download/current-version/)
    and "Download just database files: ".
    Once you have done this, unzip it and copy the "index.noun"
    file into the folder where madLib.py is located. Now
    run `python dictParse.py` and it will ask you if you
    made a copy of the supplied database as it will be overwritten.
    Then the enter the name of the database, "index.noun",
    and it will output the database ready for use. Nothing
    else is required.

Installation
------------

Installation of the python code itself is fairly straight forward.
Open a Terminal session and `cd` to the directory where you want
the PyMadLib directory to be created. Now type

`git clone https://ndeppe@bitbucket.org/ndeppe/pymadlib.git`

Next use
`cd ./PyMadLib`
to enter the directory.

Usage
-----

Note: Usage may differ on Windows.

You are now ready to generate some nonsense sentences.
The idea is really simple. You type sentence(s) and replace
any occurrence of a noun with [noun], [nouns], [noun's],
or [nouns'], depending on if you want a singular, plural,
possessive, or plural possessive noun.

To run the program open the terminal and
type `cd `, then drag the folder where
the madLib.py and database files are located and press enter.
To start the fun, type `python madLibGui.py` in the terminal.


You may also run a command line version using `python madLib.py`.
This will look like:

`user$ python madLib.py `

Type `-h` as the sentence to see help.
Enter the sentence you want to parse:

You can enter a sentence where you replace nouns by one of the following:
   [noun]   = a random noun takes this place.
   [nouns]  = a random noun followed by an s, e.g. goats takes this place.
   [noun's] = similar to above, except now it is 's. So goat's takes this place.

   [nouns'] = similar to above, except now it is plural possessive. The goats' food.

Enter the sentence you want to parse: [nouns] are like [nouns]. If you eat a [noun], I'm scared of the [noun's] [noun].


Inks are like hardboards. If you eat a elephant, I'm scared of the paste's pansy.


If you ever forget the instructions just type -h instead of a sentence.


Known Issues
------------

While developing this I noticed that getting the correct pluralizations and
word endings does take some care. I should have accounted for the majority of
cases, however there will always be some that just are exceptions to the rules.
You can add any nouns into the replacements.db file. This is just a text
file where you can specify words to be replaced either at random or by a specific
word. This is a method of dealing with edge cases. A sample replacements file
is supplied. The formatting for a randomly chosen replacement word is:
`
todays RANDOM
`
To have a specific replacement work you use:
`
mens men
`
Here the left word is the one being replaced by the word on the right.
Please be careful with capitalizations, this is largely done by itself, however
you may want to specify in certain circumstances.


Licensing
---------

Please see the file called LICENSE.

Contacts
--------

I would appreciate if no attempts to contact me outside of issues inside the
Issue Tracker were made. This is very much a hobby project and unfortunately
I do not have the time to answer any questions or make major adjustments to
the code. Suggestions are of course welcome, but there is no guarantee they
will ever be realized. Thank you for your understanding.

Credits
-------

The code and GUI was developed by Nils Deppe.

The included noun database is from
[here](http://www.desiquintans.com/articles.php?page=nounlist)
and can be freely distributed.

I would like to thank the person who first coined the phrase that started
this all. I don't know who you are, but thank you.
"Goats are like mushrooms, if you shoot a duck I'm scared of toasters."

Last but definitely not least I would like to thank
Allison Kolly for checking my instructions on Ubuntu
and Windows. Thank you!